const bodyParser = require("body-parser");
const app = require('express')();
const cors = require("cors");
const PORT = process.env.PROT || 8000;
require('./config/dbConnection');

app.use(bodyParser.urlencoded({ extended: true })).use(bodyParser.json());
app.use(cors());

const productRouter = require('./Routers/productRouter');
const orderRouter = require('./Routers/orderRouter');
const categoriesRouter = require('./Routers/categoriesRouter');

app.use('/api/categories',categoriesRouter);
app.use('/api/products',productRouter);
app.use('/api/orders',orderRouter);


app.listen(PORT, () => {console.log("Server run at port "+PORT+" !")});''