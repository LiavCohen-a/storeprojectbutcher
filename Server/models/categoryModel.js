const mongoose = require('mongoose');

let categorySchema =new mongoose.Schema({
    name : String,
    urlEnding : String
});

module.exports = mongoose.model("categories",categorySchema);