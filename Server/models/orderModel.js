const mongoose = require('mongoose');

let orderSchema =new mongoose.Schema({
    userID : String,
    date : Date,
    productsList :  [],
    sumPrice :  Number,
    isPaid :  Boolean,
    isDelivered :  Boolean,
});

module.exports = mongoose.model("orders",orderSchema);