const mongoose = require('mongoose');

let onSale =new mongoose.Schema({
    isOnSale : Boolean,
    percentageAmount : Number
});
let productSchema =new mongoose.Schema({
    name : String,
    pricePerKG : Number,
    title :  String,
    inStock :  Boolean,
    img :  String,
    description :  String,
    amount :  Number,
    onSale : onSale
});

module.exports = mongoose.model("products",productSchema);