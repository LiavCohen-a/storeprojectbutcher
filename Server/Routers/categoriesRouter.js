const categoriesBL = require('../BusinessLogic/categoriesBL');
const express = require('express');
const router = express.Router();

router.route('/').get(async function(req,resp){
    let categories = await categoriesBL.getAllCategories();
    return resp.json(categories);
});

router.route('/:id').get(async function(req,resp){
    let id = req.params.id;
    let category = await categoriesBL.getCategoryByID(id);
    return resp.json(category);
});

router.route('/').post(async function(req,resp){
    let newCategory = req.body;
    let response = await categoriesBL.addCategory(newCategory);
    return resp.json(response);
});

router.route('/:id').put(async function(req,resp){
    let id = req.params.id;
    let updatedCategory = req.body;
    let response = await categoriesBL.updateCategory(id,updatedCategory);
    return resp.json(response);
});

router.route('/:id').delete(async function(req,resp){
    let id = req.params.id;
    let response = await categoriesBL.deleteCategory(id);
    return resp.json(response);
});

module.exports = router;