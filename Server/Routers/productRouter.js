const productsBL = require('../BusinessLogic/productsBL');
const categoriesBL = require('../BusinessLogic/categoriesBL');
const express = require('express');
const router = express.Router();

router.route('/').get(async function(req,resp){
    let products = await productsBL.getAllProducts();
    return resp.json(products);
});

router.route('/byCategory').get(async function(req,resp){
    let categories = await categoriesBL.getAllCategories();
    let sortedProduct = [];
    for(const cate of categories){
      let products = await productsBL.getAllProductsByCategory(cate.name);
      sortedProduct.push(products)
    }
    return resp.json(sortedProduct);
});

router.route('/:id').get(async function(req,resp){
    let id = req.params.id;
    let product = await productsBL.getProductByID(id);
    return resp.json(product);
});

router.route('/').post(async function(req,resp){
    let newProduct = req.body;
    let response = await productsBL.addProduct(newProduct);
    return resp.json(response);
});

router.route('/:id').put(async function(req,resp){
    let id = req.params.id;
    let updatedProduct = req.body;
    let response = await productsBL.updateProduct(id,updatedProduct);
    return resp.json(response);
});

router.route('/:id').delete(async function(req,resp){
    let id = req.params.id;
    let response = await productsBL.deleteProduct(id);
    return resp.json(response);
});

module.exports = router;