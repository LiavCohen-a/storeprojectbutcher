const ordersBL = require('../BusinessLogic/ordersBL');
const express = require('express');
const router = express.Router();

router.route('/').get(async function(req,resp){
    let orders = await ordersBL.getAllOrders();
    return resp.json(orders);
});

router.route('/:id').get(async function(req,resp){
    let id = req.params.id;
    let order = await ordersBL.getOrderByID(id);
    return resp.json(order);
});

router.route('/').post(async function(req,resp){
    let newOrder = req.body;
    let response = await ordersBL.addOrder(newOrder);
    return resp.json(response);
});

router.route('/:id').put(async function(req,resp){
    let id = req.params.id;
    let updatedOrder = req.body;
    let response = await ordersBL.updateOrder(id,updatedOrder);
    return resp.json(response);
});

router.route('/:id').delete(async function(req,resp){
    let id = req.params.id;
    let response = await ordersBL.deleteOrder(id);
    return resp.json(response);
});

module.exports = router;