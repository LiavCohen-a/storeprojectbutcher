const ordersModel = require('../models/orderModel');

exports.getAllOrders = () => {
    return new Promise((resolve,reject) => {
        ordersModel.find({},(err,data) => {
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
})})};

exports.getOrderByID = function (id) {
    return new Promise((resolve, reject) => {
      ordersModel.findById(id, function (err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
})})};

exports.addOrder = function (orderData) {
    return new Promise((resolve, reject) => {
      let newOrder = ordersModel({
        userID: orderData.userID,
        date : orderData.date,
        productsList: orderData.productsList ,
        sumPrice: orderData.sumPrice ,
        isPaid: orderData.isPaid ,
        isDelivered: orderData.isDelivered
      });

      newOrder.save(function(err){
          if(err)
          {
              reject(err);
          }
          else
          {
              resolve("Order Created !")
          }
})})};

exports.updateOrder = function (id,orderUpdated) {
      return new Promise((resolve, reject) => {
        ordersModel.findByIdAndUpdate(id,orderUpdated, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve("Order Was Updated !");
          }
})})};

exports.deleteOrder = function (id) {
      return new Promise((resolve, reject) => {
        ordersModel.findByIdAndDelete(id, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve("Order Was Deleted !");
          }
})})};