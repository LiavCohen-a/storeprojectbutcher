const productsModel = require('../models/productModel');

exports.getAllProducts = () => {
    return new Promise((resolve,reject) => {
        productsModel.find({},(err,data) => {
            if(err){
                reject(err)
            }else{
                resolve(data)
            }
})})};

exports.getAllProductsByCategory = (categoryName) => {
  return new Promise((resolve,reject) => {
      productsModel.find({title : categoryName},(err,data) => {
          if(err){
              reject(err)
          }else{
              resolve(data)
          }
})})};

exports.getProductByID = function (id) {
    return new Promise((resolve, reject) => {
      productsModel.findById(id, function (err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
})})};

exports.addProduct = function (productData) {
    return new Promise((resolve, reject) => {
      let newProd = productsModel({
        name: productData.name,
        title : productData.title,
        pricePerKG: productData.pricePerKG ,
        img: productData.img ,
        inStock: productData.inStock ,
        description: productData.description ,
        amount : 0
      });
      newProd.save(function(err){
          if(err)
          {
              reject(err);
          }
          else
          {
              resolve("Product Created !")
          }
})})};

exports.updateProduct = function (id,productUpdated) {
      return new Promise((resolve, reject) => {
        productsModel.findByIdAndUpdate(id,productUpdated, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve("Product Was Updated !");
          }
})})};

exports.deleteProduct = function (id) {
      return new Promise((resolve, reject) => {
        productsModel.findByIdAndDelete(id, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve("Product Was Deleted !");
          }
})})};