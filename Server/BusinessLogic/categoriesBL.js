const categoriesModel = require('../models/categoryModel');
const productsBL = require('../BusinessLogic/productsBL');

exports.getAllCategories = () => {
    return new Promise((resolve,reject) => {
      categoriesModel.find({},async (err,data) => {
            if(err){
                reject(err)
            }else{
    
                resolve(data)
            }})
})};

exports.getCategoryByID = function (id) {
    return new Promise((resolve, reject) => {
      categoriesModel.findById(id, function (err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
})})};

exports.addCategory = function (categoryData) {
    return new Promise((resolve, reject) => {
      let newCategory = categoriesModel({
        name : categoryData.name,
        urlEnding : categoryData.urlEnding
      });

      newCategory.save(function(err){
          if(err)
          {
              reject(err);
          }
          else
          {
              resolve("Category Created !")
          }
})})};

exports.updateCategory = function (id,categoryUpdated) {
      return new Promise((resolve, reject) => {
        categoriesModel.findByIdAndUpdate(id,categoryUpdated, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve("Category Was Updated !");
          }
})})};

exports.deleteCategory = function (id) {
      return new Promise((resolve, reject) => {
        categoriesModel.findByIdAndDelete(id, function (err) {
          if (err) {
            reject(err);
          } else {
            resolve("Category Was Deleted !");
          }
})})};