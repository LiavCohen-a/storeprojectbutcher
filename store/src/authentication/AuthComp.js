//React Tools
import React from 'react';
//CSS Imports

//Components
import { useAuth0 } from "@auth0/auth0-react";

export default function AuthComp(){
    const { loginWithRedirect ,logout,user,isAuthenticated,isLoading } = useAuth0();

    return (<div>
               
                <input type={'button'} value={'Is User Login'} onClick={() => console.log(isAuthenticated) } />
                <input type={'button'} value={'Logout'} onClick={() => logout()} />
                {isLoading ? 'Loading' : console.log(user) }
           </div>
           
)};