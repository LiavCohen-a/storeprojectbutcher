import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from 'react-router-dom';
import {Auth0Provider} from '@auth0/auth0-react';
import { Provider } from "react-redux";
import { createStore } from "redux";
import appState from "./Shared/reactRedux/appState";

const appStore = createStore(appState);

ReactDOM.render(
  <BrowserRouter>
    <Provider store={appStore} >
      <Auth0Provider
        domain="dev-a7qxpkpe.us.auth0.com"
        clientId="VBA4ennIurQjXfqGYGlHfvIm7gmeWMHz"
        redirectUri={window.location.origin}
        >
        <App />
      </Auth0Provider>
      </Provider>
  </BrowserRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
