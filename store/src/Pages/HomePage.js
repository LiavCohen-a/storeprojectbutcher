import React from 'react'
import video from '../Shared/Video/VideoLogin.mp4'
//CSS
import '../Shared/CSS/LoginInterfacePage.css'
import CallIcon from '@mui/icons-material/Call';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import LinksComp from '../Shared/NavigationBar/Root/LinksComp'
import RegisterAndLogin from '../Shared/NavigationBar/Components/RegisterAndLogin';
const HomePage = () => {

    return (
        <div className="overlay">
            <div className="warp-video">
                <video className="vedio" autoPlay loop muted >
                    <source src={video} type="video/mp4" />
                </video>
            </div>
            <div><RegisterAndLogin/></div>
            <img className="title_name"src="//cdn.shopify.com/s/files/1/0360/0868/9709/files/Butchershop_Logo-whitr_150x.png?v=1584105776"></img>
            <LinksComp></LinksComp>
            <div className="co-warp">
                <a className="co-link"><CallIcon></CallIcon></a>
                <a className="co-link"><LocationOnIcon></LocationOnIcon></a>
                <a className="co-link"><FacebookIcon></FacebookIcon></a>
                <a className="co-link"><InstagramIcon></InstagramIcon></a>
            </div>
        </div>
      
       
    )
}

export default HomePage;
