//React Tools
import {useEffect, useState} from 'react';

//CSS Imports

//Components
import productService from '../Shared/Services/productService';
import ProductsComponent from '../Shared/Components/ProductsComponent';
import NavigationBar from '../Shared/NavigationBar/NavigationBar';

import axios from 'axios';
import { Outlet } from 'react-router-dom';

export default function StorePage(){

    return (
    <div>
        <NavigationBar/>
        <Outlet></Outlet>
    </div>

)};
