import React ,{useEffect,useState} from 'react'
import {useParams} from 'react-router-dom'
import ProductsComponent from '../Shared/Components/ProductsComponent';
import { useSelector } from 'react-redux';

const ProductsPage = () => {
  const {meet} = useParams();
  const [index ,setIndex] = useState(null);
  const appData = useSelector(state => state);
  useEffect(() => {
    appData.allProducts?.forEach((array,index)=> {
      if(array[0]?.title == meet){
        setIndex(index)
        }
    })
  },[meet])
    return (
        <div className="warp-pages">
          {
               appData.allProducts[index]?appData.allProducts[index].map((product)=>{
                return <ProductsComponent product={product} key={product._id}/>}):null
          }
        </div>
    )
}

export default ProductsPage
