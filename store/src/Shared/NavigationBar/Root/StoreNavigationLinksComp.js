import React from 'react'
import {Link} from 'react-router-dom'

//CSS Imports
import '../../CSS/NavBar.css'

const StoreNavigationLinksComp = () => {
    return (
        <section className="warp-links-navagation">
            <Link to="Beef" className="Link">בשר בקר</Link>
            <Link to="Chicken" className="Link">בשר עוף</Link>
            <Link to="Lamb" className="Link">כבש</Link>
            <Link to="Goose" className="Link">אווז</Link>            
        </section>
    )
}

export default StoreNavigationLinksComp;
