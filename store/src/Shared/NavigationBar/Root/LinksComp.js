//React Tools
import React , {useState,useEffect} from 'react';
import {Link} from 'react-router-dom'

//CSS Imports
import '../../CSS/NavBar.css'
//Components

export default function LinksComp(){
    return (<div className="warp-link">
                    <Link to="/Store" className="Link">לחנות מוצרים</Link>
                    <Link to="/AboutUs" className="Link">אודותינו</Link>
                    <Link to="/Review" className="Link" >ביקורות</Link>
            </div>
)};
