//React Tools
import React from 'react';
import {Routes,Route} from 'react-router-dom' ;
import AuthComp from '../../../authentication/AuthComp';
import AboutUsPage from '../../../Pages/AboutUsPage';
import StorePage from '../../../Pages/StorePage';
import HomePage from '../../../Pages/HomePage';
import ReviewPage from '../../../Pages/ReviewPage';
import ProductsPage from '../../../Pages/ProductsPage';

//Components

export default function RouterComp(){
    return (<div>
                <Routes>
                    <Route path="/"  element={<HomePage />} />
                    <Route path="/Store"  element={<StorePage />}>
                        <Route path=":meet" element={<ProductsPage/>}></Route>
                    </Route>
                    <Route path="/AboutUs"  element={<AboutUsPage />} />
                    <Route path="/Review"  element={<ReviewPage />} />
                </Routes>
           </div>
)};
