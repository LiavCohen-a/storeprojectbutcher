//React Tools 
import React from 'react'
import { useAuth0 } from "@auth0/auth0-react";
import LoginComp from './LoginComp';
import UserDataComp from './UserDataComp';


export default function RegisterAndLogin() {
    const { loginWithRedirect ,logout,user,isAuthenticated,isLoading } = useAuth0();

    return (<div className="warp-links">
        {isAuthenticated ?  <UserDataComp /> : <LoginComp />}
            </div>
)};
