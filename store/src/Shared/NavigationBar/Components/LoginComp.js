//React Tools
import React from 'react';
import { useAuth0 } from "@auth0/auth0-react";

//CSS Imports

//Components

export default function LoginComp(){
    const { loginWithRedirect } = useAuth0();

    return (  <div>
        <input type='button' className="links" value='הרשמה' onClick={() => loginWithRedirect()} />,
        <input type='button' className="links sign-up" value='כניסה' onClick={() => loginWithRedirect()} />
        </div>
)};
