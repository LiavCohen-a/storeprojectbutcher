//React Tools
import { useAuth0 } from "@auth0/auth0-react";


//CSS Imports
import {Avatar} from '@mui/material/';
import LogoutIcon from '@mui/icons-material/Logout';
//Components
export default function UserDataComp(){
    const { logout,user } = useAuth0();
    return (<div className="authenticated" >
            <LogoutIcon sx={{ width: 27, height: 27 }} className="logout" onClick={() => logout()}></LogoutIcon>
            <Avatar alt={user.name} src={user.picture} sx={{ width: 34, height: 34 }}/>
           </div>
)};
