//React Tools
import React , {useState} from 'react';

//CSS Imports
import '../../Shared/CSS/HeaderPage.css';
import '../../Shared/CSS/RegisterAndLogin.css';
import { MdOutlineRestaurantMenu } from 'react-icons/md';
import { HiMenuAlt4 } from 'react-icons/hi';

//Components
import RegisterAndLogin from '../NavigationBar/Components/RegisterAndLogin';
import ShoppingCartComp from '../NavigationBar/Components/ShoppingCartComp';
import StoreNavigationLinksComp from './Root/StoreNavigationLinksComp';

export default function NavigationHeaderComp({funShowNavBar}){

    return (
            <section id="warp-header-page">
                <div className="warp-login-cart"> 
                  <RegisterAndLogin/>
                  <ShoppingCartComp/>
                </div>
                <div>
                  <StoreNavigationLinksComp></StoreNavigationLinksComp>
                </div>
            </section>
    )
};
