import React ,{useEffect, useState } from 'react'

//CSS
import '../CSS/ProductsComponent.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function ProductsComponent({product}){
    return (
            <Card style={{ width: '18rem',height:'20rem'}} key={product.id}>
            <Card.Img variant="top" src="https://www.ynet.co.il/PicServer4/2015/04/21/6003578/579855601000100490489no.jpg" />
            <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <Card.Text>
                {product.amount}
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
            </Card.Body>
            </Card>
    )
}

