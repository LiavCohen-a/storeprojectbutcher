const appState = function(currentState = {} , action)
{
    switch (action.type) {
        case  "SaveAllProducts":
            {
                return {...currentState ,allProducts : action.payload};
            }
        default:
            return null
    }
}

export default appState;