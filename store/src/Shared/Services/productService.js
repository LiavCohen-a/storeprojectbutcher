import axios from 'axios';
let url = 'http://localhost:8000/api/products/';

//Get All Products
const getAllProducts =async () => {
  let resp =  await axios.get(url)
  return resp.data;
}
const getAllProductsByCategory =async () => {
  let resp =  await axios.get(url+"byCategory")
  return resp.data;
}
//Get Product By ID
const getProductByID = async (id) => {
    let resp =  await axios.get(url+id)
    return resp.data;
}

//Add a Product
const addProduct = async (productData) => {
  let resp =  await axios.post(url,productData)
  return resp.data;
}

//Delete Product
const deleteProduct = async (id) => {
    let resp =  await axios.post(url + id)
    return resp.data;
}

//Update Product
const updateProduct = async (id,productUpdatedData) => {
  let resp =  await axios.post(url +id,productUpdatedData)
  return resp.data;
}

export default { getAllProductsByCategory,getAllProducts ,getProductByID ,addProduct ,deleteProduct ,updateProduct};


//   const serverReqProducts = require('./Components/ServerReqProducts');

//   All Get All Products
//   const products = await serverReqProducts.getAllProducts()

//   Get a Product By ID
//   const ProductID = '61c0cf911d13bfb41e84afee'
//   const productsByID = await serverReqProducts.getProductByID(ProductID)

//   Post a New Product    *** DONT UN-Commit It will Cerate a New Product ***
//   const newProductObj = { "name": "שווארמה","pricePerKG": 40,"title": "Chiken","inStock": true,"img": "https://www.ynet.co.il/PicServer4/2015/04/21/6003578/579855601000100490489no.jpg","description": "שווארמה איכותית","amount": 0}
//   const newProduct = await serverReqProducts.addProduct(newProductObj)

//   Delete a Product     *** DONT UN-Commit It will Delete Nothing a Cerate error ***
//   const productIDForDelete = ""
//   const productDelete =  await serverReqProducts.deleteProduct(productIDForDelete)
//   console.log(productDelete)

//   Update a Product
//   const productUpdateByID ='61c0f98cc0c4981f2388cb08'
//   const newProductByID = { "name": "שווארמה","pricePerKG": 40,"title": "Chiken","inStock": true,"img": "https://www.ynet.co.il/PicServer4/2015/04/21/6003578/579855601000100490489no.jpg","description": "שווארמה איכותית","amount": 0}

//   const productUpdate = await serverReqProducts.updateProduct(productUpdateByID,newProductByID)
//   console.log(productUpdate)


