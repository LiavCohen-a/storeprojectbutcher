import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';
import RouterComp from './Shared/NavigationBar/Root/RouterComp';
import productService from './Shared/Services/productService';
import { useDispatch, useSelector } from 'react-redux';
function App() {
  const dispatch = useDispatch();
  const appData = useSelector(state => state);

  useEffect(async () => {
    let productsByCategory = await productService.getAllProductsByCategory();
    let action = {
      type : 'SaveAllProducts',
      payload : productsByCategory
    }
    dispatch(action)
  },[])

  return <div>
    
    <RouterComp />
    </div>;
}

export default App;
